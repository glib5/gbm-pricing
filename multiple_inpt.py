#import tkinter as tk
from tkinter import (Tk, LabelFrame, Label, Entry, Button)

class INPUT_Values(Tk):
    """class for getting inputs from a nice window UI"""
    
    def __init__(self, parent):
        Tk.__init__(self,parent)
        self.initialize()

    def initialize(self):
        ''' create nice UI window in with copy-paste '''
        r = 0
        
        self.grid()
        
        stepOne = LabelFrame(self, text=" 1. Enter Values ")
        stepOne.grid(row=r, columnspan=7, sticky='W',padx=5, pady=5, ipadx=5, ipady=5)

        # generic structure of 1 input
        self.L1 = Label(stepOne,text="log_2 of reps")
        self.L1.grid(row=r, column=0, sticky='E', padx=5, pady=2)
        self.i1 = Entry(stepOne)
        self.i1.grid(row=r, column=1, columnspan=3, pady=2, sticky='WE')
        r+=1

        self.L2 = Label(stepOne,text="times")
        self.L2.grid(row=r, column=0, sticky='E', padx=5, pady=2)
        self.i2 = Entry(stepOne)
        self.i2.grid(row=r, column=1, columnspan=3, pady=2, sticky='WE')
        r+=1

        self.L3 = Label(stepOne,text="1/dt")
        self.L3.grid(row=r, column=0, sticky='E', padx=5, pady=2)
        self.i3 = Entry(stepOne)
        self.i3.grid(row=r, column=1, columnspan=3, pady=2, sticky='WE')
        r+=1

        self.L4 = Label(stepOne,text="mu")
        self.L4.grid(row=r, column=0, sticky='E', padx=5, pady=2)
        self.i4 = Entry(stepOne)
        self.i4.grid(row=r, column=1, columnspan=3, pady=2, sticky='WE')
        r+=1

        self.L5 = Label(stepOne,text="sigma")
        self.L5.grid(row=r, column=0, sticky='E', padx=5, pady=2)
        self.i5 = Entry(stepOne)
        self.i5.grid(row=r, column=1, columnspan=3, pady=2, sticky='WE')
        r+=1

        self.L6 = Label(stepOne,text="s0")
        self.L6.grid(row=r, column=0, sticky='E', padx=5, pady=2)
        self.i6 = Entry(stepOne)
        self.i6.grid(row=r, column=1, columnspan=3, pady=2, sticky='WE')
        r+=1

        self.L7 = Label(stepOne,text="strike")
        self.L7.grid(row=r, column=0, sticky='E', padx=5, pady=2)
        self.i7 = Entry(stepOne)
        self.i7.grid(row=r, column=1, columnspan=3, pady=2, sticky='WE')
        r+=1

        self.L8 = Label(stepOne,text="int rate")
        self.L8.grid(row=r, column=0, sticky='E', padx=5, pady=2)
        self.i8 = Entry(stepOne)
        self.i8.grid(row=r, column=1, columnspan=3, pady=2, sticky='WE')
        r+=1

        #---
        self.val1 = None # is actually an empty str: ""
        self.val2 = None
        self.val3 = None
        self.val4 = None
        self.val5 = None
        self.val6 = None
        self.val7 = None
        self.val8 = None
        
        #---# row++
        SubmitBtn = Button(stepOne, text="Submit",command=self.submit)
        SubmitBtn.grid(row=r, column=3, sticky='W', padx=5, pady=2)

    def submit(self):
        ''' set class attribute to corresponding input '''
        self.val1 = self.i1.get() # reps
        self.val2 = self.i2.get() # times
        self.val3 = self.i3.get() # dt
        self.val4 = self.i4.get() # mu
        self.val5 = self.i5.get() # sigma
        self.val6 = self.i6.get() # s0
        self.val7 = self.i7.get() # strike
        self.val8 = self.i8.get() # r

        self.quit()


    




    
    
