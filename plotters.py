from matplotlib.pyplot import (figure, plot, errorbar, title, legend, show)


def printer(T, true, mc, mc_err):
    I = [i for i in range(len(T))]
    header = "%8s   %8s   %8s   %12s     %12s"%("","years","Black-Sholes","MC","MC error")
    print(header)
    print("_"*len(header))
    for i,t,a,b,c in zip(I, T, true, mc, mc_err):
        print("%8d | %8.4f : %8.4f   %18.4f +/- %8.4f"%(i,t,a,b,c))    


def plot1(T, true_put, mc_put_ev, mc_put_err, true_call, mc_call_ev, mc_call_err):
    print("\n\nIf strike equals initial price and interest rate is 0 then PUT and CALL HAVE the same price")
    figure()
    plot(T, true_put, label="BS", linewidth=.8)
    errorbar(T, mc_put_ev, yerr=mc_put_err, label="MC put", fmt=".")
    errorbar(T, mc_call_ev, yerr=mc_call_err, label="MC call", fmt=".", alpha=0.5, linewidth=2)
    title("European Option")
    legend()
    show()
    

def plot2(T, true_put, mc_put_ev, mc_put_err, true_call, mc_call_ev, mc_call_err):
    figure()
    plot(T, true_put, label="put", color="b", linewidth=.8)
    errorbar(T, mc_put_ev, yerr=mc_put_err, fmt=".", label="MC", color="k")
    plot(T, true_call, label="call", color="r", linewidth=.8)
    errorbar(T, mc_call_ev, yerr=mc_call_err, fmt=".", color="k")
    title("European option")
    legend()
    show()    
