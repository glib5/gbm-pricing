import numpy as np
from scipy.stats import norm

def eu_put(So, k, sigma, T, r):
    '''black sholes theorical (european) put price'''
    kT = np.exp(-r*T)*k
    k_hat = kT/So
    d = (np.log(k_hat)+ sigma*sigma*0.5*T)/sigma
    N1 = norm.cdf(d/np.sqrt(T))
    N2 = norm.cdf((d-sigma*T)/np.sqrt(T))
    return kT*N1 - So*N2

def eu_call(So, k, sigma, T, r):
    '''black sholes theorical (european) call price'''
    kT = np.exp(-r*T)*k
    k_hat = kT/So
    d = (np.log(k_hat)+ sigma*sigma*0.5*T)/sigma
    N1 = norm.cdf(d/np.sqrt(T))
    N2 = norm.cdf((d-sigma*T)/np.sqrt(T))
    return kT*(N1-1) + So*(1-N2)

def eu_put_mc(Trajs, strike, T, r):
    '''evaluates a put option
 T = times (scalar or array)
 r = interest rate (scalar/same size of T)
 Trajs = matrix of GBM trajectories'''
    kt = strike*np.exp(-r*T) 
    kT = np.full(shape=Trajs.shape, fill_value=kt.reshape(len(kt), -1))
    payoff = np.where(kT - Trajs>.0, kT - Trajs, .0)
    return payoff

def eu_call_mc(Trajs, strike, T, r):
    '''evaluates a call option
 T = times (scalar or array)
 r = interest rate (scalar/same size of T)
 Trajs = matrix of GBM trajectories'''
    kt = strike*np.exp(-r*T)
    kT = np.full(shape=Trajs.shape, fill_value=kt.reshape(len(kt), -1))
    payoff = np.where(Trajs - kT>.0, Trajs - kT, .0)
    return payoff

