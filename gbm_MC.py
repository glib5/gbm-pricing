import numpy as np
from time import perf_counter
import eu_opt

def opt_p_reps(times, reps):
    '''naive optimization of the MC geometry'''
    f = 1<<10
    t = int(12288/times) # 12288=1024*12, some magic constant
    if times<24: 
        while f<t:
            f *= 2
        if f>reps:
            f = 1<<10
    else:
        while f>t:
            f /= 2
        if f<2:
            f = 2
    return int(f)

def mc_GBM(rand, reps, times, mu, sigma, dt, s0):
    '''returns N GMB trajs, as a matrix'''
    x = np.empty(shape=(times+1, reps))
    x[1:] = (mu-.5*sigma*sigma)*dt + rand.normal(size=(times, reps), scale=np.sqrt(dt)*sigma)
    np.add.accumulate(x[1:], out=x[1:], axis=0)
    np.exp(x[1:], out=x[1:])
    x[0] = s0
    x[1:] *= s0
    return x      


def MC(rand, reps, times, mu, sigma, dt, s0, strike, T, r, p_reps):
    mc_put_ev = np.zeros(shape=(times+1), dtype=np.double)
    mc_put_err = np.zeros(shape=(times+1), dtype=np.double)
    mc_call_ev = np.zeros(shape=(times+1), dtype=np.double)
    mc_call_err = np.zeros(shape=(times+1), dtype=np.double)
    print("%20d"%reps)
    tim = perf_counter()
    for c in range(reps//p_reps):
        J = mc_GBM(rand, p_reps, times, mu, sigma, dt, s0)
        a = eu_opt.eu_put_mc(J, strike, T, r)
        mc_put_ev += np.sum(a, axis=1)
        mc_put_err += np.sum(a*a, axis=1)
        a = eu_opt.eu_call_mc(J, strike, T, r)
        mc_call_ev += np.sum(a, axis=1)
        mc_call_err += np.sum(a*a, axis=1)
        i = (c+1)*p_reps
        if i%(1<<20)==0:
            print("%20d -- %2.2f"%(i, perf_counter()-tim), end='\r', flush=True)
        elif i%(p_reps*(1<<10))==0:
            print("%12d"%i, end='\r', flush=True)
    mc_put_ev /= reps
    mc_call_ev /= reps
    conf = 3 # declared here
    mc_put_err = conf*np.sqrt((mc_put_err/reps - mc_put_ev*mc_put_ev)/reps)
    mc_call_err = conf*np.sqrt((mc_call_err/reps - mc_call_ev*mc_call_ev)/reps)
    print("\ntot MC time: %4.2f"%(perf_counter()-tim))
    return (mc_put_ev, mc_put_err, mc_call_ev, mc_call_err)
