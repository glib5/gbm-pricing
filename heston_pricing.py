from time import perf_counter as pc

from matplotlib.pyplot import figure, plot, errorbar, title, legend, show
from numba import njit
import numpy as np
from scipy.stats import norm

# G = np.random.normal(size=(2, periods, reps))

@njit
def MC_heston(reps, periods, dt, v0, k, theta, eta, rho, mu, s0, G):
    ''' returns N trajectories from an Heston model, with jumps'''
    v = np.full(shape=reps, fill_value=v0)
    X = np.empty(shape=(periods+1, reps))
    G[1] *= np.sqrt(1-rho*rho)
    X[0] = 0
    for i in range(periods):
        g = G[0][i]
        w = G[1][i]
        vdt = np.sqrt(v*dt)
        X[i+1] = X[i] + (mu - v/2)*dt + vdt*(rho*g + w)
        v += k*(theta - v)*dt + eta*vdt*g
        v = np.maximum(v, .0)
    return s0*np.exp(X)


def opt_p_reps(times, reps):
    '''naive optimization of the MC geometry'''
    f = 1<<10
    t = int(13312/times) # 1024*13, some magic constant
    if times<24: 
        while f<t:
            f *= 2
        if f>reps:
            f = 1<<10
    else:
        while f>t:
            f /= 2
        if f<2:
            f = 2
    return int(f)


def MC(rand, reps, p_reps, times, dt, v0, k, theta, eta, rho, mu, s0, strike, T, r):
    '''MC simulation for european put/call. Heston model'''
    kt = strike*np.exp(-r*T) 
    kT = np.full(shape=(times+1, p_reps), fill_value=kt.reshape(len(kt), -1)) # discount matrix
    mc_put_ev = np.zeros(shape=(times+1))
    mc_put_err = np.zeros(shape=(times+1))
    mc_call_ev = np.zeros(shape=(times+1))
    mc_call_err = np.zeros(shape=(times+1))
    print("%20d"%reps)
    tim = pc()
    for c in range(reps//p_reps):
        W = rand.normal(size=(2, times, p_reps))
        J = MC_heston(p_reps, times, dt, v0, k, theta, eta, rho, mu, s0, W)
        # put
        a = kT-J
        C = a>0
        mc_put_ev += np.sum(a, axis=1, where=C)
        mc_put_err += np.sum(a*a, axis=1, where=C)
        # call
        a = J-kT
        C = np.invert(C)
        mc_call_ev += np.sum(a, axis=1, where=C)
        mc_call_err += np.sum(a*a, axis=1, where=C)
        i = (c+1)*p_reps
        if (i%(p_reps*(1<<10)))==0:
            print("%20d -- %2.2f"%(i, pc()-tim), end='\r', flush=True)
    mc_put_ev /= reps
    mc_call_ev /= reps
    conf = 3 # declared here
    mc_put_err = conf*np.sqrt((mc_put_err/reps - mc_put_ev*mc_put_ev)/reps)
    mc_call_err = conf*np.sqrt((mc_call_err/reps - mc_call_ev*mc_call_ev)/reps)
    print("\ntotal MC time: %2.4f"%(pc()-tim))
    return (mc_put_ev, mc_put_err, mc_call_ev, mc_call_err)


def eu_th(So, k, sigma, T, r, put=True):
    '''black sholes theorical price for european option'''
    kT = np.exp(-r*T)*k
    k_hat = kT/So
    d = (np.log(k_hat)+ sigma*sigma*0.5*T)/sigma
    N1 = norm.cdf(d/np.sqrt(T))
    N2 = norm.cdf((d-sigma*T)/np.sqrt(T))
    v = kT*N1 - So*N2 if put else kT*(N1-1) + So*(1-N2)
    return v

#-----------------------------------------------------------------------

def printer(T, true, mc, mc_err):
    I = np.arange(len(T))
    header = "%8s   %8s   %8s   %12s     %12s"%("","years","Black-Sholes","MC","MC error")
    print(header)
    print("_"*len(header))
    for i,t,a,b,c in zip(I, T, true, mc, mc_err):
        print("%8d | %8.4f : %8.4f   %18.4f +/- %8.4f"%(i,t,a,b,c))    


def plot1(T, true_put, mc_put_ev, mc_put_err, true_call, mc_call_ev, mc_call_err):
    print("\n\nIf strike equals initial price and interest rate is 0 then PUT and CALL HAVE the same price")
    figure()
    plot(T, true_put, label="BS", linewidth=.8)
    errorbar(T, mc_put_ev, yerr=mc_put_err, label="MC put", fmt=".")
    errorbar(T, mc_call_ev, yerr=mc_call_err, label="MC call", fmt=".", alpha=0.5, linewidth=2)
    plot(T, mc_put_ev, linewidth=.5, color="k")
    title("European option valuation -- Heston model")
    legend()
    show()
    

def plot2(T, true_put, mc_put_ev, mc_put_err, true_call, mc_call_ev, mc_call_err):
    figure()
    plot(T, true_put, label="BS put", color="b", linewidth=.8)
    errorbar(T, mc_put_ev, yerr=mc_put_err, fmt=".", label="MC", color="k")
    plot(T, mc_put_ev, linewidth=.5, color="k")
    plot(T, true_call, label="BS call", color="r", linewidth=.8)
    errorbar(T, mc_call_ev, yerr=mc_call_err, fmt=".", color="k")
    plot(T, mc_call_ev, linewidth=.5, color="k")
    title("European option valuation -- Heston model")
    legend()
    show()    



# *** MAIN *** #
def main():
    rand = np.random.RandomState(seed=34546)
    # params
    mu = .0
    v0 = .24
    theta = .24
    sigma = .24
    k = .2
    eta = .4
    rho = -.5
    r = .02
    s0 = 4
    strike = 3.7 
    periods = int(input("#Steps? "))
    dt = 1/(float(input("dt? ")))
    reps = int(input("log_2 of reps? "))
    #--- CODE
    if reps<10:
        reps=10
    reps = 1<<reps
    p_reps = opt_p_reps(periods, reps)
    # show choosen parameters for the simulation
    print("%20s = %4d (%4d)"%("MC reps", reps, p_reps))
    print("%20s = %4.4f"%("s0", s0))
    print("%20s = %4d"%("times", periods))
    print("%20s = %4.4f"%("strike", strike))
    print("%20s = %4.4f"%("r", r))
    # Black-Sholes theorical price
    T = np.arange(0, periods+1, dtype=np.double)*dt
    true_put = eu_th(s0, strike, sigma, T[1:], r, True)
    true_put = np.insert(true_put, 0, max(.0, strike-s0))
    true_call = eu_th(s0, strike, sigma, T[1:], r, False)
    true_call = np.insert(true_call, 0, max(.0, s0-strike))
    # MC simulation
    mc_put_ev, mc_put_err, mc_call_ev, mc_call_err = MC(rand, reps, p_reps, periods, dt, v0, k, theta, eta, rho, mu, s0, strike, T, r)
    # results
    print("\nEU PUT")
    printer(T, true_put, mc_put_ev, mc_put_err)
    print("\nEU CALL")
    printer(T, true_call, mc_call_ev, mc_call_err)
    # plot
    if s0==strike and r==0:
        plot1(T, true_put, mc_put_ev, mc_put_err, true_call, mc_call_ev, mc_call_err)
    else:
        plot2(T, true_put, mc_put_ev, mc_put_err, true_call, mc_call_ev, mc_call_err)
    return 0

if __name__=="__main__":
    main()
